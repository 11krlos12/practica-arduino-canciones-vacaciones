/*
************************************************************************************************************
                                          zelda
************************************************************************************************************
*/
#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978
#define REST      0
//**********************************************************************************************************// fin zelda



/*
************************************************************************************************************
                                          zelda
************************************************************************************************************
*/
//notas y sus grados creo q asi se dice xD
int melody[] = {
  NOTE_E4, 2, NOTE_G4, 4,
  NOTE_D4, 2, NOTE_C4, 8, NOTE_D4, 8,
  NOTE_E4, 2, NOTE_G4, 4,
  NOTE_D4, -2,
  NOTE_E4, 2, NOTE_G4, 4,
  NOTE_D5, 2, NOTE_C5, 4,
  NOTE_G4, 2, NOTE_F4, 8, NOTE_E4, 8,
  NOTE_D4, -2,
  NOTE_E4, 2, NOTE_G4, 4,
  NOTE_D4, 2, NOTE_C4, 8, NOTE_D4, 8,
  NOTE_E4, 2, NOTE_G4, 4,
  NOTE_D4, -2,
  NOTE_E4, 2, NOTE_G4, 4,

  NOTE_D5, 2, NOTE_C5, 4,
  NOTE_G4, 2, NOTE_F4, 8, NOTE_E4, 8,
  NOTE_F4, 8, NOTE_E4, 8, NOTE_C4, 2,
  NOTE_F4, 2, NOTE_E4, 8, NOTE_D4, 8,
  NOTE_E4, 8, NOTE_D4, 8, NOTE_A3, 2,
  NOTE_G4, 2, NOTE_F4, 8, NOTE_E4, 8,
  NOTE_F4, 8, NOTE_E4, 8, NOTE_C4, 4, NOTE_F4, 4,
  NOTE_C5, -2,
};

int tempo = 108;
int notes = sizeof(melody) / sizeof(melody[0]) / 2;  //genera notas de 16 bits + pitch de duración
int wholenote = (60000 * 4) / tempo; //calcula la duración de la nota
int divider = 0, noteDuration = 0;



//**************************************************************************+
long DO = 523.25, //definimos las frecuencias de las notas
     DoS = 554.37,
     RE = 587.33,
     RES = 622.25,
     MI = 659.26,
     FA = 698.46,
     FAS = 739.99,
     SOL = 783.99,
     SOLS = 830.61,
     LA = 880,
     LAS = 932.33,
     SI = 987.77,
     RE2 = 1174.66,
     FAS2 = 1479.98,
     PAU = 30000; //pausa
//**************************************************************************+



/*
************************************************************************************************************
                                          noche de luna entre ruinas
************************************************************************************************************
*/
int U = 200, D = 400, T = 600, C = 800; //pausas
int melodia_1[] = {
  C,   D,   DO,  U,   MI,  U,   LA,
  U,   FA,  U,   SI,  T,   MI,  C,
  T,   SOL, T,   LA,  T,   SOL,
  D,   DO,  D,   FA,  DO,  C,   FA,
  DO,  U,   SOL, SOL, D,   DO,  D,   RE,  SOL,  U,
  DO,  C,   FA,  DO,  C,   FA,
  DO,  U,   SOL, SOL, D,   DO,  D,   RE,  U,    SOL,
  DO,  D,   SI,  D,   MI,  D,   SOL, D,
  DO,  D,   LA,  DO,  U,   DO,  D,   FA,  U,    SI,
  MI,  D,   SI,  D,   MI,  D,   SOL, D,
  DO,  D,   LA,  DO,  U,   DO,  U,   DO,  FA,   U,   SI,
  MI,  D,   DO,  U,   FA,  DO,  T,   FA,  U,
  DO,  U,   SOL, SOL, D,   DO,  D,   RE,  U,    SOL,
  DO,  D,   MI,  D,   SI,  C,   U,
  MI,  D,   DO,  D,   FA,  D,   MI,  U,   SI,
  MI,  C,   U,   SI,
  MI,  D,   DO,  D,   FA,  D,   MI,  U,   SI,
  MI,  D,   DO,  D,   SOL, D,   LA,  D,
  MI,  D,   FA,  D,   DO,  D,   DO,  D,
  SOL, D,   DO,  D,   SOL, D,   LA,  D,
  MI,  D,   FA,  D,   MI,  D,   SI,  D,
  MI,  D,   DO,  U,   SOL, T,   LA,  D,
  MI,  D,   FA,  D,   DO,  D,   DO,  D,
  SOL, D,   DO,  U,   SOL, T,   LA,  D,
  MI,  D,   FA,  D,   MI,  D,   SI,  D,
  MI,  D,   T,   SI,  D,   T,
  MI,  D,   DO,  D,   FA,  D,   MI,  U,    SI,
  MI,  D,   D,   SOL, DO,  U,   FA,  SOL,  D,
  DO,  FA,  D,   DO,  D,   D,   FA,  DO,  MI, SOL,
  D,   SOL, DO,  D,   RE,  D,   SOL, D,
  DO,  D,   T,   FA,  D,   DO,  D,
  SOL, D,   DO
};

/*
************************************************************************************************************
                                          BALADA PARA ADELINA
************************************************************************************************************
*/
int melodia_2[]={
          MI,  MI,
          MI,  FA, FA, FA,
          FA,  FA,
          FA,  FA, FA, FA, FA, FA, SOL, SOL, SOL,
          SOL, LA, MI,
          
          MI,  MI,  MI,
          MI,  MI,  MI, MI, MI, FA, FA, FA,
          FA,  FA,
          FA,  FA,  FA, FA, FA, FA, SOL, SOL, SOL,
          
          DO,  SI,  LA,
          SI,  LA,  SOL,
          LA,  SOL, FA,  MI,
          DO,  SI,  LA,
          SI,  LA,  SOL,
          LA,  SOL, FA,  SOL,
          
          MI,  MI, MI,
          MI,  MI, MI, MI, MI, FA, FA, FA,
          FA,  FA,
          FA,  FA, FA, FA, FA, FA, SOL, SOL, SOL,
          SOL, LA, MI,
        };







/*
************************************************************************************************************
                                          CONFIGURACIÓN PINES
************************************************************************************************************
*/
const int pinBuzzer = 9;
const int siguiente = 7;
const int anterior = 6;
int contador = 0;
bool salida_1=false, salida_2=false, salida_3=false;






void setup() {
  pinMode(siguiente, INPUT_PULLUP);
  pinMode(anterior, INPUT_PULLUP);
  contador = 0;
  salida_1 = false;
  salida_2 = false;
  salida_3 = false;
}

void loop() {

  int sig = digitalRead(siguiente);
  int ant = digitalRead(anterior);

  if (sig == HIGH || ant == HIGH) {
    musiconZelda(true);
    musiconMarimba(true);
    musiconAdelina(true);
    

    delay(1000);
    if (sig == HIGH) {
      contador++;
    } else {
      contador--;
    }

    


    if (contador < 5) 
    {
      if (contador == 1) {
        musiconZelda(false);
        //musiconMarimba(false);
      } else if (contador == 2) {
        musiconZelda(false);
      } else if(contador == 3){
        musiconAdelina(false);
      }
    }
    else 
    {
      contador = 1;
    }


  }


}



void musiconZelda(bool salida) {
  for (int thisNote = 0; thisNote < notes * 2; thisNote = thisNote + 2) {
    if(salida == true){
      return;
    }
    divider = melody[thisNote + 1];  //calcula la duración de las demas notas en el array
    if (divider > 0) {
      noteDuration = (wholenote) / divider; //regula la nota para que suene bien justo en el proceso de sonar
    } else if (divider < 0) {
      noteDuration = (wholenote) / abs(divider);  //notas altas, aumeto de la misma al inicio
      noteDuration *= 1.5; // aumenta aum mas la intensidad de la misma nota justo a la mitad de su duración
    }

    // we only play the note for 90% of the duration, leaving 10% as a pause
    tone(pinBuzzer, melody[thisNote], noteDuration * 0.9);

    // Wait for the specief duration before playing the next note.
    delay(noteDuration);

    // stop the waveform generation before the next note.
    noTone(pinBuzzer);
  }
}

void musiconMarimba(bool salida) {
  for (int actual = 0; actual < 250; actual++) {
    if(salida==true){
      return;
    }
    tone(9, melodia_1[actual]);
    if (melodia_1[actual] == U  ||  melodia_1[actual] == D  || melodia_1[actual] == T  || melodia_1[actual] == C) {
      delay(melodia_1[actual]);
      noTone(9);
    }
    noTone(9);
  }
}

void musiconAdelina(bool salida){
  for(int actual =0; actual < 86; actual++){
    if(salida){
      return;
    }
    tone(9, melodia_2[actual]);
    delay(150);
    noTone(9);
  }
}
